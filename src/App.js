import {Filter} from './components/Filter';
import {Forecast} from "./components/Forecast";
import {CurrentWeather} from "./components/CurrentWeather";
import {Today} from "./components/Today";

import dayjs from "dayjs";
require('dayjs/locale/uk');
dayjs.locale('uk');

export const App = () => {
    return (
        <main>
            <Filter />
            <Today />
            <CurrentWeather />
            <Forecast />
        </main>
    );
};
