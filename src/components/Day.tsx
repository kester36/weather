interface DayWeather {
    "weakday": string,
    "temperature": number,
    "type": string
}

export const Day: React.FC<DayWeather> = (props) => {

    return (
        <div className={`day ${props.type}`}>
            <p>{props.weakday}</p>
            <span>{props.temperature}</span>
        </div>
    )
}
