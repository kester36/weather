export const Filter: React.FC = () => {
    return (
        <div className="filter">
            <span className="checkbox">Хмарно</span>
            <span className="checkbox selected">Сонячно</span>
            <p className="custom-input">
                <label htmlFor="min-temperature">Мінімальна температура</label>
                <input id="min-temperature" type="text" />
            </p>
            <p className="custom-input">
                <label htmlFor="min-temperature">Максимальна температура</label>
                <input id="max-temperature" type="text" />
            </p>
            <button>Відфильтровати</button>
        </div>
    );
}