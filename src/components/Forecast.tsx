import dayjs from "dayjs"
import forecast from '../mock-data/forecast.json'

import {Day} from "./Day"

export function Forecast() {
    const Days = forecast
        .slice(0, 7)
        .map((dayWeather) => {
            const dayOfWeek = dayjs(dayWeather.day).format('dddd');

            return (
                <Day
                    weakday={dayOfWeek}
                    temperature={dayWeather.temperature}
                    type={dayWeather.type}
                />
            )
        })

    return <div className="forecast">
        {Days}
    </div>
}
