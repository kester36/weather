import dayjs from "dayjs"

export function Today() {
    return <div className="head">
        <div className="icon cloudy"/>
        <div className="current-date">
            <p>{dayjs().format('dddd')}</p>
            <span>{dayjs().format('D MMMM')}</span>
        </div>
    </div>;
}